﻿#include <iostream>
#include "Helpers.h"

class Animal
{
public:
    virtual void Voice()
    {}
};

class Dog : public Animal
{
private:
    std::string x = "Гав";
public:
    void Voice() override
    {
        std::cout << x << "\n";
    }
};

class Cat : public Animal
{
private:
    std::string x = "Мяу";
public:
    void Voice() override
    {
        std::cout << x << "\n";
    }
};

class Cow : public Animal
{
private:
    std::string x = "Муу";
public:
    void Voice() override
    {
        std::cout << x << "\n";
    }
};

int main()
{
    setlocale(LC_ALL, "Russian");

    Animal* animals[3];

    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Cow();

    for (Animal* a : animals)
        a->Voice();

    return 0;
}

